DROP DATABASE IF EXISTS practica3;
CREATE DATABASE IF NOT EXISTS practica3;
USE practica3;

CREATE TABLE notas(
  id int AUTO_INCREMENT,
  fecha date,
  hora time,
  mensaje varchar(255),
  PRIMARY KEY(id)   
  );