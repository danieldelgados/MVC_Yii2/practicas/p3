<?php
/* @var $this yii\web\View */

$this->title = 'Práctica 3';
?>
<div class="site-index">



    <?php
    foreach ($fechas as $fecha) {
        echo"<h2>".date("d F Y",strtotime($fecha->fecha))."</h2>";
        foreach ($notas as $nota) {

            if ($fecha->fecha == $nota->fecha) {
                echo $this->render('_mensajes', [
                    "nota" => $nota,
                ]);
            }
        }
    }
    ?>
</div>
